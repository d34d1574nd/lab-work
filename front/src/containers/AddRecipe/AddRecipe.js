import React, {Component} from 'react';
import {Button, Col, Form, FormGroup} from "reactstrap";
import FormElement from "../../components/UI/Form/FormElement";

class AddRecipe extends Component {
  state = {
    title: '',
    image: null,
    description: ''
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    })
  };

  fileChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.files[0]
    })
  };

  submitFormHandler = event => {
    event.preventDefault();
  };

  render() {
    return (
      <div>
        <Form onSubmit={this.submitFormHandler}>
          <FormElement
            propertyName="title"
            title="title"
            type="text"
            value={this.state.title}
            onChange={this.inputChangeHandler}
            placeholder="Enter title recipe"
          />

          <FormElement
            propertyName="image"
            title="image"
            type="file"
            onChange={this.fileChangeHandler}
          />

          <FormElement
            propertyName="description"
            title="description"
            type="textarea"
            value={this.state.description}
            onChange={this.inputChangeHandler}
            placeholder="Enter description"
          />

          <FormGroup row>
            <Col sm={{offset: 3, size: 9}}>
              <Button type="submit" color="primary">
                Add recipe
              </Button>
            </Col>
          </FormGroup>
        </Form>
      </div>
    );
  }
}

export default AddRecipe;
