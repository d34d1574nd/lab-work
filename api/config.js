const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, 'public/uploads'),
  dbUrl: process.env.NODE_EVN === 'test' ? 'mongodb://localhost/shop_test' : 'mongodb://localhost/shop',
  mongoOptions: {
    useNewUrlParser: true,
    useCreateIndex: true
  },
  facebook: {
    appId: '1175316582633061',
    appSecret: '6855fce7960da090d2c62ce150d7228e'
  }
};
