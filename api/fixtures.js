const mongoose = require('mongoose');
const config = require('./config');

const Users = require('./models/User');
const Recipes = require('./models/Recipes');

const run = async () => {
  await mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for (let collection of collections) {
    await collection.drop();
  }

  const users = await Users.create({
    username: 'user',
    password: '123',
    role: 'user',
    token: nanoid()
  }, {
    username: "admin",
    password: '123',
    role: "admin",
    token: nanoid()
  });

  await Recipes.create(
    {user: users[0], title: 'Ramen', description: 'Recipe cooking dfsdfdhg  gfd vdv  v v dvs dvsd vs dfsas sfsa f saf', image: 'ramen.jpeg'}
    {user: users[0], title: 'Ramen', description: 'Recipe cooking dfsdfdhg  gfd vdv  v v dvs dvsd vs dfsas sfsa f saf', image: 'ramen.jpeg'}
    {user: users[0], title: 'Ramen', description: 'Recipe cooking dfsdfdhg  gfd vdv  v v dvs dvsd vs dfsas sfsa f saf', image: 'ramen.jpeg'}
    {user: users[0], title: 'Ramen', description: 'Recipe cooking dfsdfdhg  gfd vdv  v v dvs dvsd vs dfsas sfsa f saf', image: 'ramen.jpeg'}
  )

  return connection.close();
};


run().catch(error => {
  console.error('Something wrong happened...', error);
});
